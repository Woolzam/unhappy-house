package com.ssafy.happyhouse.model.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssafy.happyhouse.model.UserDto;
import com.ssafy.happyhouse.model.UserFavoriteDto;
import com.ssafy.happyhouse.model.mapper.UserMapper;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper usermapper;
	
	@Override
	public UserDto selectOneUserById(String user_id) throws Exception {
		return usermapper.selectOneUserById(user_id);
	}

	@Override
	public List<UserDto> selectAll() throws Exception {
		return usermapper.selectAll();
	}
	
	@Override
	public Boolean insert(UserDto user)  throws Exception{
		return usermapper.insert(user);
		
	}

	@Override
	public Boolean update(UserDto user)  throws Exception{
		return usermapper.update(user);
		
	}

	@Override
	public Boolean delete(String user_id) throws Exception {
		return usermapper.delete(user_id);
		
	}
	
	@Override
	public List<UserFavoriteDto> selectFavorite(String user_id) throws Exception {
		
		return usermapper.selectFavorite(user_id);
	}
	
	@Override
	public String getdongcode(String dongname) throws Exception {
		
		return usermapper.getdongcode(dongname);
	}

	@Override
	public String getdongname(String dongCode)  throws Exception{
	
		return usermapper.getdongname(dongCode);
	}

	@Override
	public Boolean insertFavorite(UserFavoriteDto dto) throws Exception {
		return usermapper.insertFavorite(dto);
	}

	@Override
	public Boolean deleteFavorite(UserFavoriteDto dto) throws Exception {
		return usermapper.deleteFavorite(dto);
	}


	@Override
	public Boolean deleteFavoriteAll(String user_id) throws SQLException {
		return usermapper.deleteFavoriteAll(user_id);
	}

	@Override
	public UserDto login(String user_id, String user_pwd) throws SQLException {
		
		return usermapper.login(user_id, user_pwd);
	}

	@Override
	public void qnaDelete(String user_id) throws SQLException {
		usermapper.qnaDelete(user_id);
		
	}

	@Override
	public void boardDelete(String user_id) throws SQLException {
		usermapper.boardDelete(user_id);
		
	}

	@Override
	public UserDto selectOneUserByEmail(String email) throws SQLException {
		
		return usermapper.selectOneUserByEmail(email);
	}

	@Override
	public void replyDelete(String user_id) throws SQLException {
	
		 usermapper.replyDelete(user_id);
	}
}
