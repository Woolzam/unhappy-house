package com.ssafy.happyhouse.model.service;

import java.sql.SQLException;
import java.util.List;

import com.ssafy.happyhouse.model.UserDto;
import com.ssafy.happyhouse.model.UserFavoriteDto;

public interface UserService {
	public UserDto selectOneUserById(String user_id) throws Exception;
	public UserDto selectOneUserByEmail(String email) throws SQLException;
	public List<UserDto> selectAll() throws Exception;
	public Boolean insert(UserDto user) throws Exception;
	public Boolean update(UserDto user) throws Exception;
	public Boolean delete(String user_id) throws Exception;
	public List<UserFavoriteDto> selectFavorite(String user_id) throws Exception;
	public String getdongcode(String dongname) throws Exception;
	public String getdongname(String dongCode) throws Exception;
	public Boolean insertFavorite(UserFavoriteDto dto) throws Exception;
	public Boolean deleteFavorite(UserFavoriteDto dto) throws Exception;
	public Boolean deleteFavoriteAll(String user_id) throws Exception;
	public UserDto login(String user_id, String user_pwd) throws SQLException;
	public void qnaDelete(String user_id) throws SQLException;
	public void boardDelete(String user_id) throws SQLException;
	public void replyDelete(String user_id) throws SQLException;
}
