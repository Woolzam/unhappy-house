package com.ssafy.happyhouse.model.mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.ssafy.happyhouse.model.UserDto;
import com.ssafy.happyhouse.model.UserFavoriteDto;

@Mapper
public interface UserMapper {
	public UserDto selectOneUserById(String user_id) throws SQLException;
	public UserDto selectOneUserByEmail(String email) throws SQLException;
	public List<UserDto> selectAll() throws SQLException;
	public Boolean insert(UserDto user) throws SQLException;
	public Boolean update(UserDto user) throws SQLException;
	public Boolean delete(String user_id) throws SQLException;
	public List<UserFavoriteDto> selectFavorite(String user_id) throws SQLException;
	public String getdongcode(String dongname) throws SQLException;
	public String getdongname(String dongCode) throws SQLException;
	public Boolean insertFavorite(UserFavoriteDto dto) throws SQLException;
	public Boolean deleteFavorite(UserFavoriteDto dto) throws SQLException;
	public Boolean deleteFavoriteAll(String user_id) throws SQLException;
	public UserDto login(String user_id, String user_pwd) throws SQLException;
	public void qnaDelete(String user_id) throws SQLException;
	public void boardDelete(String user_id) throws SQLException;
	public void replyDelete(String user_id) throws SQLException;
}
