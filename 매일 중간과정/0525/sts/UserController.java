package com.ssafy.happyhouse.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.ssafy.happyhouse.model.UserDto;
import com.ssafy.happyhouse.model.UserFavoriteDto;
import com.ssafy.happyhouse.model.service.BoardService;
import com.ssafy.happyhouse.model.service.QnaService;
import com.ssafy.happyhouse.model.service.UserService;
import com.ssafy.happyhouse.util.BoolResult;
import com.ssafy.happyhouse.util.HashUtil;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = {"*"}, maxAge = 6000)
@RestController
@RequestMapping("/user")
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@ApiOperation(value="유저 정보를 등록한다.",response =  BoolResult.class)
	@RequestMapping(value="/register",method=RequestMethod.POST)
	public ResponseEntity<BoolResult> register(@RequestBody UserDto user) throws Exception{
		logger.info("register");

		boolean set = userService.insert(user);
		BoolResult nr = new BoolResult();
		nr.setCount(set);
		nr.setName("insertUser");
		nr.setState("succ");
		
		if(!set) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<BoolResult>(nr,HttpStatus.OK);
	}
	
	@ApiOperation(value="모든 유저의 정보를 반환한다.", response=List.class)
	@RequestMapping(value="/selectAll",method=RequestMethod.GET)
	public ResponseEntity<List<UserDto>> selectAll() throws Exception{
		logger.info("selectAll");
		List<UserDto> users = userService.selectAll();
		if(users.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<List<UserDto>>(users, HttpStatus.OK);
	} 

	@ApiOperation(value="유저의 아이디로 정보를 찾는다.",response = UserDto.class)
	@RequestMapping(value="/selectOneUserById/{user_id}",method=RequestMethod.GET)
	public ResponseEntity<UserDto> selectOneUserById(@PathVariable String user_id) throws Exception{
		logger.info("selectOneUserByid");
		UserDto user = userService.selectOneUserById(user_id);
		
		if(user==null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<UserDto>(user, HttpStatus.OK);
	}
	
	@ApiOperation(value="유저의 이메일로 정보를 찾는다.",response = UserDto.class)
	@RequestMapping(value="/selectOneUserByEmail/{email}",method=RequestMethod.GET)
	public ResponseEntity<UserDto> selectOneUserByEmail(@PathVariable String email) throws Exception{
		logger.info("selectOneUserByEmail");
		UserDto user = userService.selectOneUserByEmail(email);
		
		if(user==null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<UserDto>(user, HttpStatus.OK);
	}
	
	

	@ApiOperation(value="아이디로 유저정보를 수정한다.",response=BoolResult.class)
	@RequestMapping(value="/update", method=RequestMethod.PUT)
	public ResponseEntity<BoolResult> update(@RequestBody UserDto user) throws Exception{
		logger.info("update");
		
		boolean set = userService.update(user);
		BoolResult nr = new BoolResult();
		nr.setCount(set);
		nr.setName("updateUser");
		nr.setState("succ");
		
		if(!set) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<BoolResult>(nr,HttpStatus.OK);
	}
	

	
	@ApiOperation(value="아이디로 유저정보를 삭제한다.", response = BoolResult.class)
	@RequestMapping(value="/delete/{user_id}",method = RequestMethod.DELETE)
	public ResponseEntity<BoolResult> delete(@PathVariable String user_id) throws Exception{ 
		logger.info("delete");
		
		userService.boardDelete(user_id);
		userService.qnaDelete(user_id);
		userService.deleteFavoriteAll(user_id);
		userService.replyDelete(user_id);
		boolean set = userService.delete(user_id);
		BoolResult nr = new BoolResult();
		nr.setCount(set);
		nr.setName("deleteUser");
		nr.setState("succ");
		
		if(!set) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<BoolResult>(nr, HttpStatus.OK);
	}

	@ApiOperation(value="아이디와 비밀번호로 로그인한다.", response = UserDto.class)
	@RequestMapping(value="/login",method = RequestMethod.POST)
	public ResponseEntity<UserDto> login(@RequestBody UserDto user) throws Exception{ 

		UserDto user2 = userService.login(user.getUser_id(), user.getUser_pwd());
		
		if(user2.getPhone() == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<UserDto>(user2,HttpStatus.OK);
	}
	
	@ApiOperation(value="유저의 아이디로 관심지역을 찾는다.",response = List.class)
	@RequestMapping(value="/selectFavorite/{user_id}",method=RequestMethod.GET)
	public ResponseEntity<List<UserFavoriteDto>> selectFavorite(@PathVariable String user_id) throws Exception{
		logger.info("selectFavorite");
		
		List<UserFavoriteDto> favorites = userService.selectFavorite(user_id);
		
		if(favorites.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<UserFavoriteDto>>(favorites, HttpStatus.OK);
	}
	

	@ApiOperation(value="아이디로 관심지역을 삭제한다.", response = BoolResult.class)
	@RequestMapping(value="/deleteFavorite/{user_id}/{dongcode}",method = RequestMethod.DELETE)
	public ResponseEntity<BoolResult> deleteFavorite(@PathVariable String user_id, @PathVariable String dongcode) throws Exception{ 
		logger.info("deleteFavorite");
		UserFavoriteDto ufd = new UserFavoriteDto(user_id,dongcode);
		
		boolean set = userService.deleteFavorite(ufd);
		BoolResult nr = new BoolResult();
		nr.setCount(set);
		nr.setName("deleteFavorite");
		nr.setState("succ");
		
		if(!set) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<BoolResult>(nr, HttpStatus.OK);
	}

	@ApiOperation(value="관심지역을 등록한다.",response =  BoolResult.class)
	@RequestMapping(value="/insertFavorite/{user_id}/{dongcode}",method=RequestMethod.POST)
	public ResponseEntity<BoolResult> insertFavorite(@PathVariable String user_id, @PathVariable String dongcode) throws Exception{
		logger.info("insertFavorite");
		UserFavoriteDto ufd = new UserFavoriteDto(user_id,dongcode);
		
		boolean set= false;
		BoolResult nr = new BoolResult();
		
		set = userService.insertFavorite(ufd);
		nr.setCount(set);
		nr.setName("insertFavorite");
		nr.setState("succ");

		if(!set) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<BoolResult>(nr,HttpStatus.OK);
	}

}
